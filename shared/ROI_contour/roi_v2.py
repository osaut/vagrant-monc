# -*- coding: utf-8 -*-
"""
Created on Fri Jan 16 11:18:03 2015

@author: aperetti
"""

# -*- coding: utf-8 -*-
"""
Created on Thu Jan 15 15:03:04 2015

@author: aperetti
"""

import dicom
from roi import ROI
from vtkImage import vtkImage
import pylab
from sklearn import mixture
import numpy as np




#BOUTY
#nom_patient="Bouty"
#numero_tumeur="1"
#date="20141226"


nom_patient="Pla"
numero_tumeur="3"
date="20140505"

#GAG
#nom_patient="Gag"
#numero_tumeur="1"
#date="20120606"



nom_fichier_xml=nom_patient+numero_tumeur+"_"+date+"_"+"plugin.xml"
nom_fichier_AF=nom_patient+numero_tumeur+"_"+date+"_"+"AF.dcm"

print(nom_fichier_xml)
print(nom_fichier_AF)

nom_new_dcm = "new"+nom_fichier_AF

#filtre avec la methode gaussian smooth

image=vtkImage.from_file(nom_fichier_AF)
#nom_gaussian_smooth_dcm="gaussian"+nom_fichier_AF

gaussian_dcm=image.gaussian_smooth()

median_dcm=image.median_filter()


def texture_roi(nom_fichier_xml,nom_dicom,nom_new_dcm):
    
    "genere le masque autour de la ROI et fait la liste des intensites \
    incluses la roi dans la variable intensites"
#    roi=ROI(nom_fichier_xml)
    roi=ROI("./"+nom_patient+"_Roi_plugin/"+nom_fichier_xml)
#    roi=ROI("./" + nom_fichier_xml)
    roi.write_as_vtp("test.vtp")
    
    #image=vtkImage.from_file("test_ex.dcm")
    image=nom_dicom
    image_masque=image.mask_with_roi(roi)
    image_gradient=image_masque.gradient_filter()
    
    
    image.write_as_vti("image.vti")
    image_masque.write_as_vti("test.vti")
    image_gradient.write_as_vti("gradient.vti")
    
    # Necessaire pour replacer les modifs dans un DICOM complet
    ds=dicom.read_file(image.dcmfile)

    ds.pixel_array
    # Modification
    np=image_masque.as_narray()
    print('len',len(np.flat))
    
 #    sauvegarde des intensites dans la ROI
    intensites=[]
#    for n, val in enumerate(np.flat):
    for val in np.flat :    
#    si l intensite est differente de -1000 on est sur un pixel de la ROI
#   on en stocke l intensite dans la liste intensites
        if val != -1000 :
            intensites.append(val)       
        
    ds.PixelData=np.tostring()
    ds.save_as(nom_new_dcm)
    pylab.imshow(ds.pixel_array, cmap=pylab.cm.bone)
    pylab.show()
    
    return (intensites,np,ds)
    
#appel avec le filtre
(intensites,Np,Ds)=texture_roi(nom_fichier_xml,gaussian_dcm,nom_new_dcm)

#appel sans le filtre
#(intensites,Np,Ds)=texture_roi(nom_fichier_xml,image,nom_new_dcm)


#il ne faut pas s amuser a trier les intensites par ordre croissant ou autre
#sinon l appartenance aux gaussiennes est fausse !

print('len intensites',len(intensites))
#print('intensites',intensites)

for k in range (0,len(intensites)):
    intensites[k]=int(intensites[k])
nb_intensites=len(intensites)


#occurence relative de chaque intensite dans la liste intensites avec un dico

occurence={}.fromkeys(set(intensites),0)
for valeur in intensites :
    occurence[valeur]+=1.0/nb_intensites
#print(occurence)

        
couples=occurence.items()
nb_I_distinctes=len(couples)
print ('nombre d intensites distinctes' ,nb_I_distinctes)

    
#utilisation de GMM pour avoir plusieurs gaussiennes
intensites_array=np.array(intensites)[:, np.newaxis]
a=int(min(intensites_array))
b=int(max(intensites_array))
N=int(max(intensites_array))-int(min(intensites_array))
N_plot_gaussiennes=2500
X_plot = np.linspace(a,b,N+1)[:, np.newaxis]

#X_plot_gaussiennes=np.linspace(a,b,N_plot_gaussiennes+1)[:, np.newaxis]
X_plot_gaussiennes=np.linspace(a-250,b+250,3000)[:, np.newaxis]
X_plot_gaussiennes=np.linspace(600,1200,3000)[:, np.newaxis]

nb_gaussiennes_max=4

#on teste l'approximation par differents nb de gaussiennes pour garder le nb
#correspondant a l'erreur min



#parametres des gaussiennes
liste_sigma_carre=[]
liste_mu=[]
liste_pi=[]

#valeurs des AIC et BIC pour chaque nb de gaussiennes
valeurs_AIC=[]
valeurs_BIC=[]

for j in range (1,nb_gaussiennes_max+1) :
    
    clf=mixture.GMM(n_components=j,n_iter=800,thresh=0.001,n_init=10)
    kdebis=clf.fit(intensites_array)
    print('AIC',clf.aic(intensites_array))
    print('BIC',clf.bic(intensites_array))
    print('*'*50)
    #parametres des j gaussiennes trouvees
    for i in range (0,j):
        print('nb de gaussiennes total recherchees',j)
        print('gaussienne numero',i)
        print('moyenne',clf.means_[i][0])        
        print('sigma_carre',clf.covars_[i][0])
        print('poids',clf.weights_[i])    
        print('*'*50)

        liste_sigma_carre.append(clf.covars_[i][0])
        liste_mu.append(clf.means_[i][0])
        liste_pi.append(clf.weights_[i])
    
    valeurs_AIC.append(clf.aic(intensites_array))    
    valeurs_BIC.append(clf.bic(intensites_array)) 

def gaussienne(x,i,j):
    "expression des gaussiennes calculees avec fit \
    j est un entier indiquant le nb de gaussiennes cherchees\
    i est dans l'intervalle [0,j-1]"
    
    #les param de la gaussienne i pour n gaussiennes cherchees sont localisees 
    #en n dans les listes
    
    n=(j-1)*j/2+i
    
    sigma_carre=liste_sigma_carre[n]
    mu=liste_mu[n]
    pi=liste_pi[n]
                
    g=np.exp((-(x-mu)**2)/(2.0*sigma_carre))\
    *(1.0/(np.sqrt(2.0*np.pi*sigma_carre)))*pi    
    return g
            
def composee_gaussiennes(x,j):
    "expression de la somme des gaussiennes obtenues avec fit\
    j est l'entier indiquant le nb de gaussiennes cherchees"
    somme=0.0
    for i in range(0,j):
        somme+=gaussienne(x,i,j)
    return somme    


#la liste erreur contient les ererurs entre l'histo et les j gaussiennes
#on choisira le nb de gaussiennes j correspondant a l'erreur minimale
erreur=[]

#calcul de l'erreur entre la somme des gaussiennes et l'histogramme
#epsilon=10**-3
#epsilon*(j-1) permet d'indiquer qu'on prefere approximer par le moins 
#de gaussiennes possible
for j in range (1,nb_gaussiennes_max+1):
    erreur_somme=0.0
    for k in range(0,len(couples)):
        erreur_somme+=(composee_gaussiennes(couples[k][0],j)-couples[k][1])**2
#    erreur.append(np.sqrt(erreur_somme)+epsilon*(j-1))
    erreur.append(np.sqrt(erreur_somme)+valeurs_AIC[j-1]/10000.\
    +2.0*valeurs_BIC[j-1]/10000.)
    
print('erreur',erreur)
#l'indice j0 tel que erreur(j) est min donne le nb de gaussiennes necessaires
#on ajoute 1 car les indices de la liste commencent a 0 
j0=erreur.index(min(erreur))+1
print('Nb de gaussiennes utilisees',j0)    


# appartenance de chaque pixels aux gaussiennes
def appartenance_pixel(X,j):
    
    " determination de la gaussienne a laquelle les pixels dont les intensites \
     sont dans la liste X appartiennent, j designe le nombre de gaussienne qui optimise l erreur"
     
    appartenance=[]
    appartenance_max_gaussienne=[]       
     #faire un test si j==1 car il n'y a qu'une seule gaussienne
    if j==1 :
        print("il n'y a qu'une seule gaussienne donc tous les pixels lui appartiennent")

        for indice_intensite in range(0,len(X)) :
                appartenance.append(0)
                mu=liste_mu[0]
                appartenance_max_gaussienne.append(gaussienne(mu,0,1))
    else:

        for indice_intensite in range(0,len(X)) :
            comparaison=[]
            for k in range (0,j) :
                comparaison.append(gaussienne(X[indice_intensite],k,j))
            appartenance.append(comparaison.index(max(comparaison)))
            
            #on evalue la gaussienne choisie en mu pour avoir sa valeur max
            n=(j-1)*j/2+appartenance[indice_intensite]
            mu=liste_mu[n]
            appartenance_max_gaussienne.append(gaussienne(mu,appartenance[indice_intensite],j))

    return (appartenance,appartenance_max_gaussienne)

appartenance=appartenance_pixel(intensites,j0)


def modif_pixels_ROI(x,y,z,nom):
    "modifie les intensites de la roi contenues dans y.flat selon l appartenance\
    aux gaussiennes z. la sauvegarde de l image se fait avec ds, ici appele x \
    plus les pixels sont fonces sur l image modifiee\
    plus leur gaussienne d appartenance a un numero eleve"
    
    compteur=-1
    
    for i in range (0,len(y.flat)):
        if y.flat[i]!=-1000:
            compteur+=1
            y.flat[i]=(z[compteur])*1000
            
    x.PixelData=y.tostring()
    x.save_as(nom)
    pylab.title(nom)
    pylab.imshow(x.pixel_array, cmap=pylab.cm.bone)
    pylab.show()

#trace
#normalisation de l'histogramme
weights=[]
for i in range (0,nb_intensites):
    weights.append(1.0/nb_intensites)
weights=np.array(weights)    

pylab.figure()

#modification de la taille de la legende sur le graphe
params_graphe = {'legend.fontsize': 10}
pylab.rcParams.update(params_graphe)

#trace de chaque gaussienne pour le nb de gaussiennes approprie
for k in range (0,j0):
    pylab.plot(X_plot_gaussiennes,gaussienne(X_plot_gaussiennes,k,j0),label="gaussienne"+str(k))
    
 
#trace de la composee des gaussiennes pr le nb de gaussiennes approprie
pylab.plot(X_plot_gaussiennes,composee_gaussiennes(X_plot_gaussiennes,j0), ':r',label="composee")


#print('appartenance',appartenance[1])
pylab.plot(intensites,appartenance[1],'o',label="appartenance")

#trace de l histogramme
pylab.hist(intensites_array,bins=N,facecolor='black',weights=weights)

#affichage de la legende
pylab.legend()
pylab.ylabel('occurences relatives')
pylab.xlabel('intensites')
pylab.title(nom_fichier_xml)


#affichage du graphe
pylab.show()

#modification de l intensite des pixels de la ROI en fonction de leur 
#appartenance aux gaussiennes

modif_pixels_ROI(Ds,Np,appartenance[0],"appartenance"+nom_patient+numero_tumeur+"_"+date+".dcm")





