# Environnement de développement MONC

L'objectif est de définir un environnement de travail portable sous différentes configurations. On installe un système minimal (Ubuntu Utopic Unicorn, 378MB) qui tourne sur des hôtes différents. La config de l'environnement de travail est donc indépendante de la configuration de l'hôte.

## Installation
L'installation ne demande pas de prérequis à part [Homebrew](http://brew.sh) pour les Mac. Les instructions précises sont les suivantes :

1. Installer [VirtualBox](https://www.virtualbox.org/).
2. Installer [Vagrant](http://www.vagrantup.com/).
3. Installer [Ansible](http://www.ansible.com/home) par la commande
    `brew install ansible` (Mac)
ou
    `sudo apt-get install ansible` (Debian/Ubuntu)
4. Aller dans n'importe quel sous-dossier contenant un clone du dépôt. Les codes utilisant ITK seront donc dans un sous-répertoire.
5. Lancer ``vagrant up`` dans un terminal dans ce répertoire. La box est alors chargée et les packages installés (cela peut prendre un certain temps).

### Installation plus spécifique sur Mac
Sur Mac, on peut simplifier l'installation et tout faire avec [Homebrew](http://brew.sh) et [Homebrew Cask](http://caskroom.io).

    brew install caskroom/cask/brew-cask
    brew cask install virtualbox
    brew cask install vagrant
    brew install ansible

et en bonus une appli pour la barre des menus :

    brew install Caskroom/cask/vagrant-bar

qui est installée dans `~/Applications`. Pour utiliser `iTerm` comme terminal, exécuter

    defaults write bipsync.Vagrant-Bar terminalAppName -string iTerm

en ligne de commande. Pour lancer l'application au démarrage, la rajouter dans `Préférences Systèmes...> Utilisateurs et Groupes > Ouverture`.

## Utilisation pour développer
1. Dans un sous-répertoire du répertoire contenant le `Vagrantfile` lancer la box avec `vagrant up`. La box est alors lancée et on peut se connecter dessus.
2. Dans un terminal, se connecter à la box avec `vagrant ssh`. On est alors connecté sur la machine comme si elle était distante. C'est depuis cette connexion qu'il faudra lancer les compilations...
3. Quand on a fini, quitter la box et éteindre avec `vagrant halt` ou `vagrant suspend`. On peut la redémarrer avec `vagrant reload`.

Pour développer, le plus pratique est d'accéder aux fichiers avec son éditeur préféré côté hôte. Les fichiers  sont dans le dossier `shared` du dossier contenant le `Vagrantfile` sur l'hôte. Le même dossier est accessible depuis la box au chemin `shared`.

Une fois connecté sur la box, on est connecté sous le nom d'utilisateur `vagrant` et le répertoire utilisateur correspondant (`/home/vagrant`) n'est visible que dans la box.

## Détails
Le fichier `Vagrantfile` contient la description de la box (l'image du système initial). Actuellement c'est une version Ubuntu Utopic. Au premier lancement de la box (`vagrant up`), le script d'installation `build.sh` est exécuté (comme root). Pour relancer le script il suffit de faire `vagrant provision`.

Les packages installés (et qui devraient marcher...) sont :

- [Jansson](http://www.digip.org/jansson/)
- [CMake](http://www.cmake.org)
- [Eigen](http://eigen.tuxfamily.org)
- [Numpy](http://www.numpy.org)
- [VTK](http://www.vtk.org) + interface Python
- [ITK](http://www.itk.org) + interface Python
- [SimpleITK](http://www.simpleitk.org)
- [Scikit-Learn](http://scikit-learn.org)
- [pydicom](https://github.com/darcymason/pydicom)

Les logiciels suivants sont compilés à partir des sources à la provision de la VM :

- [Cadmos](https://bitbucket.org/osaut/cadmos)

Par mettre à jour les outils VirtualBox automatiquement à chaque redémarrage de la box, on peut rajouter le plugin à Vagrant par

    vagrant plugin install vagrant-vbguest

### Configuration de la box
La box est configurable dans le `Vagrantfile` dans la partie :

    config.vm.provider :virtualbox do |vb|
            vb.customize ["modifyvm", :id, "--memory", 4096]
            vb.customize ["modifyvm", :id, "--cpus", 2]
            vb.customize ["modifyvm", :id, "--ioapic", "on"]
    end

 Pour l'instant, elle est donc limitée à 2 CPUs et 4Go de mémoire mais au besoin on peut augmenter ces valeurs.

#### Accès `ssh`
Dans la VM, les accès `ssh` utilisent le forwarding

    config.ssh.forward_agent = true

c'est-à-dire que par exemple pour accéder à une dépôt sur Bitbucket, la clé de l'utilisateur **sur l'hôte** sera utilisée.

Pour forwarder X11, il faut en plus rajouter la ligne

    config.ssh.forward_x11 = true

Ce n'est pas fait par défaut.

#### Dossier partagé
Le dossier partagé est configurable par la ligne

        config.vm.synced_folder "./shared", "/shared"

du `Vagrantfile`. Le premier argument est le nom d'un dossier sur l`hôte et le deuxième est le chemin par lequel il sera accessible sur la box.

### Compilation d'ITK
Pour compiler ITK (et le wrapper Python) à partir des sources au lieu d'installer les paquets, il faut changer la ligne

    build_itk: false

du Vagrantfile pour mettre

    build_itk: true

à la place. La provision charge alors les sources d'ITK 4.7 et tente de les compiler. Pour compiler une autre version, il suffit de changer la variable `itk_version` dans le fichier `roles/itk-src/defaults/main.yml`.

### Notes sur Python & ITK
#### ITK seul

En principe, l'appel des routines ITK depuis Python devrait marcher. Il y a juste quelques warnings qui peuvent être supprimés en rajoutant

    import warnings
    warnings.filterwarnings("ignore")

au début du script Python important itk.

#### Interactions avec NumPy
##### SimpleITK & NumPy
Les interactions NumPy avec SimpleITK devraient fonctionner. On peut convertir d'une image SITK à un tableau numpy (et réciproquement) avec les appels

    import numpy
    import SimpleITK as sitk
    # ...
    nparray=sitk.GetArrayFromImage(image)

et

    import numpy
    import SimpleITK as sitk
    # ...
    img=sitk.GetImageFromArray(nparray)

##### ITK & NumPy
La conversion ITK / NumPy nécessite un module externe à ITK (qui n'est pas construit par défaut) : [ITKBridgeNumPy](https://github.com/InsightSoftwareConsortium/ITKBridgeNumPy). Quand la box est construite avec les sources d'ITK (mettre `build_itk: true` dans le `VagrantFile`), un checkout du module est fait dans les sources d'ITK et il est construit. Par défaut il n'est pas dans les paquets binaires.

La compilation est très longue... *Le module construit n'a pas encore été testé!*

###### ITK & NumPy via SimpleITK
Une image SimpleITK contient un image ITK. En C++, il y a le constructeur correspondant. On pourrait alors imaginer interfacer ITK & NumPy en convertissant les images ITK en SimpleITK d'abord. Pour une raison inconnue, cela ne marche pas en Python.

###### ITK & NumPy à la main
Il y a au moins deux solutions :

- Dans ITK on a accès aux valeurs de pixels d'une image, on pourrait s'en servir pour construire le tableau NumPy correspondant à la main.
Une routine C++ faisant quelque chose d'équivalent est dans Cadmos : [src/extras/dicom.cc](https://bitbucket.org/osaut/cadmos/src/457a03181a0b8ae1351d2fb402a187bb5d0c02a9/src/extra/dicom.cc?at=master).

- Passer par les fichiers DICOM. Lire ou écrire les fichiers avec [pydicom](https://github.com/darcymason/pydicom) pour les transformer en tableaux NumPy.

## Box personnalisée

Dès que l'on a une box satisfaisante, après provision par exemple, on peut en faire une image pour la charger sans avoir à refaire toutes les installations, compilations...

Pour cela,

    vagrant global-status

renvoie le nom des différentes VM actuellement sur l'ordinateur. Après il suffit de choisir l'id de celle que l'on veut et faire

    vagrant package _id_

Vagrant éteindra alors la box correspondante et en fera une image avec compression. On peut alors enregistrer l'image avec Vagrant pour pouvoir l'utiliser :

    vagrant box add --name moncbox _image_.box

 On peut alors utiliser directement l'image avec un `Vagrantfile` du type

    Vagrant.configure("2") do |config|
        config.vm.box = "moncbox"

    end

et échanger l'image avec ses amis pour qu'ils fassent de même.
